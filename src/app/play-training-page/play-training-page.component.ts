import { TrainingService } from '../shared/services/training.service';
import { TrainingComponent } from '../training/training.component';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TrainingModel } from '../shared/models/training.model';
import { ActivatedRoute } from '@angular/router';
import { ExerciseModel } from '../shared/models/exercise.model';

@Component({
  selector: 'app-play-training-page',
  templateUrl: './play-training-page.component.html',
  styleUrls: ['./play-training-page.component.scss']
})
export class PlayTrainingPageComponent implements OnInit {

  @Input()
  public trainingModel: TrainingModel;

  @ViewChild('trainingComponent')
  public trainingComponent: any;

  private trainingId: string;

  constructor(private activatedRoute: ActivatedRoute,
    private trainingService: TrainingService) {
    this.trainingModel = new TrainingModel();
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (params['trainingId'] !== undefined) {
        this.trainingId = params['trainingId'];
        this.getTraining();
      }
    });
  }

  private getTraining() {
    this.trainingService.getTraining(this.trainingId).then((trainingResult) => {
      this.trainingModel = trainingResult;
      this.trainingComponent.refresh();
    }).catch((error) => {
      console.log(error);
    });
  }

}

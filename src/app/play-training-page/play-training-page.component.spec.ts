import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayTrainingPageComponent } from './play-training-page.component';

describe('TrainingContainerPageComponent', () => {
  let component: PlayTrainingPageComponent;
  let fixture: ComponentFixture<PlayTrainingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayTrainingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayTrainingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

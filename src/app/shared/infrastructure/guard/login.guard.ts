import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router) {  }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      return this.protectPagesIfUserIsntLogged();
    }

  private protectPagesIfUserIsntLogged() {
    if (localStorage.getItem('currentUserEmail')) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }
}

import { UserTrainingModel } from './../models/user-training.model';
import { TrainingService } from './training.service';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private trainingService: TrainingService) {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    (Parse as any).serverURL = environment.serverURL;
  }

  public enrollUserInTraining(userId: string, trainingId: string): Promise<any> {
    const UserToTrainingObject = Parse.Object.extend('UserToTraining');
    const userToTraining = new UserToTrainingObject();

    userToTraining.set('UserId', userId);
    userToTraining.set('TrainingId', trainingId);

    return new Promise<any>((resolve, reject) => {
      userToTraining.save()
      .then((object) => {
        resolve(object);
      }, (error) => {
        reject(error);
      });
    });
  }

  public removeUserFromTraining(userId: string, trainingId: string): Promise<any> {
    const userToTraining = new Parse.Query('UserToTraining');
    userToTraining.equalTo('UserId', userId);
    userToTraining.equalTo('TrainingId', trainingId);

    return new Promise<any>((resolve, reject) => {
      userToTraining.first()
      .then((object) => {
        object.destroy().then(() => {
          resolve();
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  public saveUserExerciseProgress(userId: string, exerciseId: string, currentBPM: number): Promise<any> {
    const UserExerciseProgressObject = Parse.Object.extend('UserExerciseProgress');
    const userExerciseProgress = new UserExerciseProgressObject();

    userExerciseProgress.set('UserId', userId);
    userExerciseProgress.set('ExerciseId', exerciseId);
    userExerciseProgress.set('CurrentBPM', currentBPM);

    return new Promise<any>((resolve, reject) => {
      userExerciseProgress.save()
      .then((object) => {
        resolve(object);
      }, (error) => {
        reject(error);
      });
    });
  }

  public getTrainingsByUser(userId: string): Promise<UserTrainingModel[]> {
    const userTotrainings = new Parse.Query('UserToTraining');
    userTotrainings.equalTo('UserId', userId);

      return new Promise<any>((resolve, reject) => {
        userTotrainings.find().then((userToTrainings) => {
          const userToTrainingsAtt = userToTrainings
            .reduce((att, userToTraining) => [ ...att, ...userToTraining.attributes ], []);
          const trainingIds = userToTrainingsAtt
            .reduce((id, training) => [ ...id, ...training.TrainingId ], []);

          this.trainingService.getTrainings(trainingIds).then((trainingResults) => {
            const toReturn = [];
            trainingResults.forEach(trainingResult => {
              const userTraining = new UserTrainingModel();
              userTraining.TrainingId = trainingResult.Id;
              userTraining.TrainingName = trainingResult.attributes.Name;
              userTraining.UserId = userId;
              toReturn.push(userTraining);
            });
            resolve(toReturn);
          });
        }, (error) => {
          reject(error);
        });
      });
  }

  public getUserByUserEmail(userEmail: string): Promise<UserModel> {
    const user = new Parse.Query('User');
    user.equalTo('email', userEmail);

    return new Promise<UserModel>((resolve, reject) => {
      user.first().then((userReturned) => {
        const userToReturn = new UserModel();
        userToReturn.Id = userReturned.id;
        userToReturn.Email = userReturned.attributes.email;
        userToReturn.UserName = userReturned.attributes.username;
        resolve(userToReturn);
      }, ((error) => {
        reject(error);
      }));
    });
  }
}

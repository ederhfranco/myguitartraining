import { TrainingModel } from './../models/training.model';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import * as Parse from 'parse';
import { ExerciseModel } from '../models/exercise.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor() {
    Parse.initialize(environment.PARSE_APP_ID, environment.PARSE_JS_KEY);
    (Parse as any).serverURL = environment.serverURL;
  }

  public saveTraining(trainingModel: TrainingModel): Promise<any> {
    const TrainingObject = Parse.Object.extend('Training');
    const trainings = new TrainingObject();

    trainings.set('Name', trainingModel.Name);
    trainings.set('IsPrivate', trainingModel.IsPrivate);

    return new Promise<any>((resolve, reject) => {
      trainings.save().then((trainingSaved) => {
        trainingModel.Exercises.forEach((exercise) => {
          this.saveExercise(exercise).then((exerciseSaved) => {
            this.saveExerciseToTraining(exerciseSaved.id, trainingSaved.id).then(() => {
              // TODO: Chamar o método saveUserExerciseProgress();
              resolve(trainingSaved);
            });
          });
        });
      }, (error) => {
        reject(error);
      });
    });
  }

  public updateTraining(trainingModel: TrainingModel): Promise<any> {
    const Training = Parse.Object.extend('Training');
    const query = new Parse.Query(Training);

    return new Promise<any>((resolve, reject) => {
      query.get(trainingModel.Id).then((object) => {
        object.set('Name', trainingModel.Name);
        object.set('IsPrivate', trainingModel.IsPrivate);
        object.save().then((trainingUpdated) => {
          trainingModel.Exercises.forEach((exercise) => {
            const isNewExercise = exercise.Id === undefined || exercise.Id === null;
            if (isNewExercise) {
              this.saveExercise(exercise).then((exerciseSaved) => {
                this.saveExerciseToTraining(exerciseSaved.id, trainingUpdated.id).then(() => {
                });
              });
            } else {
              this.updateExercise(exercise);
            }
          });
          resolve(trainingUpdated);
        }, (error) => {
          reject(error);
        });
      });
    });
  }

  public saveExercise(exerciseModel: ExerciseModel): Promise<any> {
    const ExerciseObject = Parse.Object.extend('Exercise');
    const exercises = new ExerciseObject();

    exercises.set('Name', exerciseModel.Name);
    exercises.set('ImageUrl', exerciseModel.ImageUrl);
    exercises.set('Minutes', exerciseModel.Minutes);
    exercises.set('Seconds', exerciseModel.Seconds);
    exercises.set('BeatsPerMinute', exerciseModel.BeatsPerMinute);
    exercises.set('HasMetronome', exerciseModel.HasMetronome);
    exercises.set('OrderIndex', exerciseModel.OrderIndex);
    exercises.set('BpmToIncrease', exerciseModel.BpmToIncrease);
    exercises.set('ImageFile', exerciseModel.ImageFile);
    console.log(exerciseModel);

    return new Promise<any>((resolve, reject) => {
      exercises.save()
      .then((object) => {
        resolve(object);
      }, (error) => {
        reject(error);
      });
    });
  }

  public updateExercise(exerciseModel: ExerciseModel): Promise<any> {
    const query = new Parse.Query('Exercise');
    return new Promise<any>((resolve, reject) => {
      query.get(exerciseModel.Id).then((object) => {
        object.set('Name', exerciseModel.Name);
        object.set('ImageUrl', exerciseModel.ImageUrl);
        object.set('Minutes', exerciseModel.Minutes);
        object.set('Seconds', exerciseModel.Seconds);
        object.set('BeatsPerMinute', exerciseModel.BeatsPerMinute);
        object.set('HasMetronome', exerciseModel.HasMetronome);
        object.set('OrderIndex', exerciseModel.OrderIndex);
        object.set('BpmToIncrease', exerciseModel.BpmToIncrease);
        object.set('ImageFile', exerciseModel.ImageFile);
        object.save()
          .then((response) => {
            resolve(response);
          }, (error) => {
            reject(error);
        });
      });
    });
  }

  public saveExerciseToTraining(exerciseId: string, trainingId: string) {
    const ExerciseToTrainingObject = Parse.Object.extend('ExerciseToTraining');
    const exerciseToTraining = new ExerciseToTrainingObject();

    exerciseToTraining.set('ExerciseId', exerciseId);
    exerciseToTraining.set('TrainingId', trainingId);

    return new Promise<any>((resolve, reject) => {
      exerciseToTraining.save().then((object) => {
        resolve(object);
      }, (error) => {
        reject(error);
      });
    });
  }

  public getTraining(trainingId: string): Promise<any> {
      const trainings = new Parse.Query('Training');

      return new Promise<any>((resolve, reject) => {
        trainings.get(trainingId).then((training) => {
          const trainingResult = training.attributes;
          const trainingToReturn = new TrainingModel();
          trainingToReturn.Id = training.id;
          trainingToReturn.Name = trainingResult.Name;
          trainingToReturn.IsPrivate = trainingResult.IsPrivate;
          this.buildExercisesToTraining(training.id).then((exercisesResults) => {
            trainingToReturn.Exercises = exercisesResults;
            trainingToReturn.CurrentExercise = trainingToReturn.Exercises[0];
          }).catch((error) => {
            reject(error);
          }).then(() => {
            resolve(trainingToReturn);
          });
        }, (error) => {
          reject(error);
        });
      });
  }

  public getTrainings(trainingIds: string[]): Promise<any> {
    const trainings = new Parse.Query('Training');
    trainings.containedIn('objectId', trainingIds);

    return new Promise<any>((resolve, reject) => {
      trainings.find().then((trainingResults) => {
        const trainingsToReturn = [];
        trainingResults.forEach(trainingResult => {
          const training = {
            Id: trainingResult.id,
            attributes: trainingResult.attributes,
          };
          trainingsToReturn.push(training);
        });
        resolve(trainingsToReturn);
      }, ((error) => {
        reject(error);
      }));
    });
  }

  public getPublicTrainings(): Promise<any> {
    const trainings = new Parse.Query('Training');
    trainings.equalTo('IsPrivate', false);

    return new Promise<any>((resolve, reject) => {
      trainings.find().then((trainingResults) => {
        const toReturn = trainingResults
          .reduce((att, training) => [ ...att, ...training.attributes ], []);
        resolve(toReturn);
      }, ((error) => {
        reject(error);
      }));
    });
  }

  public getExercise(exerciseId: string): Promise<ExerciseModel> {
    const exercises = new Parse.Query('Exercise');

    return new Promise<any>((resolve, reject) => {
      exercises.get(exerciseId).then((exercise) => {
        const exerciseResult = exercise.attributes;
        const exerciseToReturn = new ExerciseModel;
        exerciseToReturn.Id = exercise.id;
        exerciseToReturn.Name = exerciseResult.Name;
        exerciseToReturn.ImageUrl = exerciseResult.ImageUrl;
        exerciseToReturn.Minutes = exerciseResult.Minutes;
        exerciseToReturn.Seconds = exerciseResult.Seconds;
        exerciseToReturn.BeatsPerMinute = exerciseResult.BeatsPerMinute;
        exerciseToReturn.HasMetronome = exerciseResult.HasMetronome;

        resolve(exerciseToReturn);
      }, (error) => {
        reject(error);
      });
    });
  }

  public getExercises(exerciseIds: string[]): Promise<ExerciseModel[]> {
    const exercises = new Parse.Query('Exercise');
    exercises.containedIn('objectId', exerciseIds);

    return new Promise<ExerciseModel[]>((resolve, reject) => {
      exercises.find().then((exerciseResults) => {

        const toReturn = [];
        exerciseResults.forEach(exerciseResult => {
          const exerciseModel = new ExerciseModel();
          exerciseModel.Id = exerciseResult.id;
          exerciseModel.BeatsPerMinute = exerciseResult.attributes.BeatsPerMinute;
          exerciseModel.BpmToIncrease = exerciseResult.attributes.BpmToIncrease;
          exerciseModel.HasMetronome = exerciseResult.attributes.HasMetronome;

          if (exerciseResult.attributes.ImageUrl !== undefined &&
              exerciseResult.attributes.ImageUrl !== null &&
              exerciseResult.attributes.ImageUrl !== '') {
            exerciseModel.ImageUrl = exerciseResult.attributes.ImageUrl;
          } else {
            exerciseModel.ImageUrl =
            exerciseResult.attributes.ImageFile && exerciseResult.attributes.ImageFile._url  ?
            exerciseResult.attributes.ImageFile._url : '';
          }

          exerciseModel.Minutes = exerciseResult.attributes.Minutes;
          exerciseModel.Name = exerciseResult.attributes.Name;
          exerciseModel.OrderIndex = exerciseResult.attributes.OrderIndex;
          exerciseModel.Seconds = exerciseResult.attributes.Seconds;
          toReturn.push(exerciseModel);
        });

        const results = toReturn
          .sort((a, b) => {
            if (a.OrderIndex > b.OrderIndex) {
              return 1;
            }
            if (a.OrderIndex < b.OrderIndex) {
                return -1;
            }
            return 0;
          });
        resolve(results);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  public getAllExercises(): Promise<ExerciseModel[]> {
    const exercises = new Parse.Query('Exercise');

    return new Promise<ExerciseModel[]>((resolve, reject) => {
      exercises.find().then((exerciseResults) => {

        const toReturn = [];
        exerciseResults.forEach(exerciseResult => {
          const exerciseModel = new ExerciseModel();
          exerciseModel.Id = exerciseResult.id;
          exerciseModel.BeatsPerMinute = exerciseResult.attributes.BeatsPerMinute;
          exerciseModel.BpmToIncrease = exerciseResult.attributes.BpmToIncrease;
          exerciseModel.HasMetronome = exerciseResult.attributes.HasMetronome;
          // exerciseModel.ImageUrl = exerciseResult.attributes.ImageUrl;
          exerciseModel.ImageUrl =
            exerciseResult.attributes.ImageFile && exerciseResult.attributes.ImageFile._url  ?
            exerciseResult.attributes.ImageFile._url : '';
          exerciseModel.Minutes = exerciseResult.attributes.Minutes;
          exerciseModel.Name = exerciseResult.attributes.Name;
          exerciseModel.OrderIndex = exerciseResult.attributes.OrderIndex;
          exerciseModel.Seconds = exerciseResult.attributes.Seconds;
          toReturn.push(exerciseModel);
        });

        const results = toReturn
          .sort((a, b) => {
            if (a.OrderIndex > b.OrderIndex) {
              return 1;
            }
            if (a.OrderIndex < b.OrderIndex) {
                return -1;
            }
            return 0;
          });
        resolve(results);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  public async getExercisesToTraining(trainingId: string): Promise<any> {
    const exercisesToTraining = new Parse.Query('ExerciseToTraining');
    exercisesToTraining.equalTo('TrainingId', trainingId);

    return new Promise<any>((resolve, reject) => {
      exercisesToTraining.find().then((exercisesToTrainingResults) => {
        const toReturn = exercisesToTrainingResults.reduce((att, exercise) => [ ...att, ...exercise.attributes ], []);
        resolve(toReturn);
      }, (error) => {
        reject(error);
      });
    });
  }

  public removeExerciseFromTraining(exerciseId: string, trainingId: string): Promise<any> {
    const exercisesToTraining = new Parse.Query('ExerciseToTraining');
    exercisesToTraining.equalTo('ExerciseId', exerciseId);
    exercisesToTraining.equalTo('TrainingId', trainingId);

    const ExerciseToTraining = Parse.Object.extend('ExerciseToTraining');
    const query = new Parse.Query(ExerciseToTraining);

    return new Promise<any>((resolve, reject) => {
      exercisesToTraining.find().then((result) => {
        query.get(result[0].id).then((object) => {
          object.destroy().then((response) => {
            resolve();
          }, (error) => {
            reject();
          });
        });
      });
    });
  }

  public parseImageFileBase64ToParseFile(imageFileBase64: any, fileName: string): any {
    return new Parse.File(fileName, { base64: imageFileBase64 });
  }

  public saveUserExerciseProgress(userId: string, exerciseId: string, currentBPM: number): Promise<any> {
    const UserExerciseProgress = Parse.Object.extend('UserExerciseProgress');
    const myNewObject = new UserExerciseProgress();

    myNewObject.set('UserId', userId);
    myNewObject.set('ExerciseId', exerciseId);
    myNewObject.set('CurrentBPM', currentBPM);

    return new Promise<any>((resolve, reject) => {
      myNewObject.save().then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  private buildExercisesToTraining(trainingId: string): Promise<ExerciseModel[]> {
    let exercisesToReturn: ExerciseModel[] = [];
    return new Promise<ExerciseModel[]>((resolve, reject) => {
      this.getExercisesToTraining(trainingId).then((exerciseToTrainingResult) => {
        const exerciseIds = exerciseToTrainingResult.reduce((ids, exerciseToTraining) => [ ...ids, exerciseToTraining.ExerciseId ], []);
        this.getExercises(exerciseIds).then((exerciseResults) => {
          exercisesToReturn = exerciseResults;
          resolve(exercisesToReturn);
        });
      }).catch((error) => {
        reject(error);
      });
    });
  }

}

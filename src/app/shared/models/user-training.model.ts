export class UserTrainingModel {
    private userId: string;
    public get UserId() {
        return this.userId;
    }
    public set UserId(value: string) {
        this.userId = value;
    }

    private trainingId: string;
    public get TrainingId() {
        return this.trainingId;
    }
    public set TrainingId(value: string) {
        this.trainingId = value;
    }

    private trainingName: string;
    public get TrainingName() {
        return this.trainingName;
    }
    public set TrainingName(value: string) {
        this.trainingName = value;
    }
}

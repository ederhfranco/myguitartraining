export class ExerciseModel {

    private id: string;
    public get Id() {
        return this.id;
    }
    public set Id(value: string) {
        this.id = value;
    }

    private name: string;
    public get Name() {
        return this.name;
    }
    public set Name(value: string) {
        this.name = value;
    }

    private imageUrl = '';
    public get ImageUrl() {
        return this.imageUrl;
    }
    public set ImageUrl(value: any) {
        this.imageUrl = value;
    }

    private minutes = 0;
    public get Minutes() {
        return this.minutes;
    }
    public set Minutes(value: number) {
        this.minutes = value;
    }

    private seconds = 0;
    public get Seconds() {
        return this.seconds;
    }
    public set Seconds(value: number) {
        this.seconds = value;
    }

    private beatsPerMinute = 0;
    public get BeatsPerMinute() {
        return this.beatsPerMinute;
    }
    public set BeatsPerMinute(value: number) {
        this.beatsPerMinute = value;
    }

    private hasMetronome = this.BeatsPerMinute > 0;
    public get HasMetronome() {
        return this.BeatsPerMinute > 0;
    }
    public set HasMetronome(value: boolean) {
        this.hasMetronome = value;
    }

    private orderIndex: number;
    public get OrderIndex() {
        return this.orderIndex;
    }
    public set OrderIndex(value: number) {
        this.orderIndex = value;
    }

    private bpmToIncrease = 0;
    public get BpmToIncrease() {
        return this.bpmToIncrease;
    }
    public set BpmToIncrease(value: number) {
        this.bpmToIncrease = value;
    }

    private imageFile: any;
    public get ImageFile() {
        return this.imageFile;
    }

    public set ImageFile(value: any) {
        this.imageFile = value;
    }

    private minutesText: string;
    private secondsText: string;
    public get Duration() {
        this.minutesText = this.minutes < 10 ? '0' + this.minutes.toString() : this.minutes.toString();
        this.secondsText = this.seconds < 10 ? '0' + this.seconds.toString() : this.seconds.toString();
        return `${this.minutesText}:${this.secondsText}`;
    }

    public IsInitialized(): boolean {
        return this.Name !== undefined && this.Name !== null && this.Name !== '';
        // return this.Name !== undefined && this.Name !== null && this.Name !== '' &&
        //     this.ImageUrl !== undefined && this.ImageUrl !== null && this.ImageUrl !== '';
    }

    public IsValid(): boolean {
        return this.IsInitialized() &&
            (this.minutes >= 0 && this.seconds > 0) ||
            (this.minutes > 0 && this.seconds >= 0);
    }
}

import { ExerciseModel } from './exercise.model';
export class TrainingModel {

    private id: string;
    public get Id() {
        return this.id;
    }
    public set Id(value: string) {
        this.id = value;
    }

    private name: string;
    public get Name() {
        return this.name;
    }
    public set Name(value: string) {
        this.name = value;
    }

    private exercises: ExerciseModel[];
    public get Exercises() {
        return this.exercises;
    }
    public set Exercises(value: ExerciseModel[]) {
        this.exercises = value;
    }

    private currentExerciseIndex = 0;
    private currentExercise: ExerciseModel;
    public get CurrentExercise() {
        return this.currentExercise;
    }
    public set CurrentExercise(value: ExerciseModel) {
        this.currentExercise = value;
    }

    private isPrivate: boolean;
    public get IsPrivate() {
        return this.isPrivate;
    }
    public set IsPrivate(value: boolean) {
        this.isPrivate = value;
    }

    public get AmountOfExercises() {
        return this.exercises.length;
    }

    constructor() {
        this.exercises = [];
        this.currentExercise = new ExerciseModel();
    }

    public SetNextExercise(): boolean {
        if (this.exercises.length - 1 === this.currentExerciseIndex) {
            return true;
        }

        if (this.exercises.length > this.currentExerciseIndex) {
            this.currentExerciseIndex += 1;
            this.currentExercise = this.exercises[this.currentExerciseIndex];
        }

        return false;
    }

    public SetPreviousExercise() {
        if (this.currentExerciseIndex > 0) {
            this.currentExerciseIndex -= 1;
            this.currentExercise = this.exercises[this.currentExerciseIndex];
        }
    }

    public SortExercises() {
        this.Exercises = this.Exercises
        .sort((a, b) => {
            if (a.OrderIndex > b.OrderIndex) {
              return 1;
            }
            if (a.OrderIndex < b.OrderIndex) {
                return -1;
            }
            return 0;
          });
        this.CurrentExercise = this.Exercises[0];
    }
}

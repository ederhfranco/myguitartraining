export class UserModel {
    private objectId: string;
    public get Id() {
        return this.objectId;
    }
    public set Id(value: string) {
        this.objectId = value;
    }
    private username: string;
    public get UserName() {
        return this.username;
    }
    public set UserName(value: string) {
        this.username = value;
    }
    private email: string;
    public get Email() {
        return this.email;
    }
    public set Email(value: string) {
        this.email = value;
    }
    private password: string;
    public get Password() {
        return this.password;
    }
    public set Password(value: string) {
        this.password = value;
    }
}

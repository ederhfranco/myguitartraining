import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { ExerciseModel } from '../shared/models/exercise.model';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit {

  @Output()
  private finishedExerciseEvent: EventEmitter<any> = new EventEmitter();

  @Input()
  public exerciseModel: ExerciseModel;

  @ViewChild('timerComponent')
  public timerComponent: any;

  @ViewChild('metronomeComponent')
  public metronomeComponent: any;

  public showLoading = false;

  constructor() {
    this.exerciseModel = new ExerciseModel();
  }

  ngOnInit() {
  }

  public play() {
    this.metronomeComponent.start();
    this.timerComponent.start();
    console.log(this.exerciseModel);
  }

  public pause() {
    this.metronomeComponent.stop();
    this.timerComponent.pause();
  }

  public refreshSoft() {
    setTimeout(() => {
      this.timerComponent.refresh(this.exerciseModel.Minutes, this.exerciseModel.Seconds);
    }, 10);
  }

  public refresh() {
    this.showLoading = true;
    this.metronomeComponent.refresh();
    // this.metronomeComponent.stop();
    if (this.exerciseModel.HasMetronome) {
      this.metronomeComponent.start();
    } else {
      this.metronomeComponent.stop();
    }
    this.timerComponent.pause();
    this.timerComponent.refresh(this.exerciseModel.Minutes, this.exerciseModel.Seconds);

    setTimeout(() => {
      this.showLoading = false;
      this.timerComponent.start();
      // this.metronomeComponent.start();
    }, 4000);
  }

  public finishedExerciseEmitMethod() {
    console.log('Chegamos no exercise.component');
    this.finishedExerciseEvent.emit();
  }

}

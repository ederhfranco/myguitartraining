import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTrainingPageComponent } from './register-training-page.component';

describe('RegisterTrainingPageComponent', () => {
  let component: RegisterTrainingPageComponent;
  let fixture: ComponentFixture<RegisterTrainingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterTrainingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTrainingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

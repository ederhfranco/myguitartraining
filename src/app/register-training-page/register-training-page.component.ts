import { UserService } from './../shared/services/user.service';
import { TrainingService } from './../shared/services/training.service';
import { TrainingModel } from './../shared/models/training.model';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatIconRegistry, MatSnackBar } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ExerciseModel } from '../shared/models/exercise.model';
import { ExternalReference } from '@angular/compiler';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-register-training-page',
  templateUrl: './register-training-page.component.html',
  styleUrls: ['./register-training-page.component.scss']
})
export class RegisterTrainingPageComponent implements OnInit {

  private userEmail: string;
  private trainingId: string;
  private exerciseIdsToDelete = [];
  public imageUploadCloud = './../../assets/cloud.png';
  public trainingModel: TrainingModel;
  public newExercise: ExerciseModel;
  public selectedTabIndex = 0;
  public newExerciseVisible = false;
  public trainingTabDisabled = false;

  constructor(private activatedRoute: ActivatedRoute,
    private trainingService: TrainingService,
    private userService: UserService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private snackBar: MatSnackBar,
    private router: Router) {
      iconRegistry.addSvgIcon(
        'electric-guitar',
        sanitizer.bypassSecurityTrustResourceUrl('assets/electric-guitar.svg'));
      iconRegistry.addSvgIcon(
        'add',
        sanitizer.bypassSecurityTrustResourceUrl('assets/add.svg'));
    }


  ngOnInit() {
    this.trainingModel = new TrainingModel();
    this.newExercise = new ExerciseModel();

    this.userEmail = localStorage.getItem('currentUserEmail');

    this.activatedRoute.params.subscribe((params) => {
      if (params['trainingId'] !== undefined) {
        this.trainingId = params['trainingId'];
        this.getTraining();
      }
    });
  }

  private getTraining() {
    this.trainingService.getTraining(this.trainingId).then((trainingResult) => {
      this.trainingModel = trainingResult;
    }).catch((error) => {
      console.log(error);
    });
  }

  private fillNewExercise(exerciseModel: ExerciseModel) {
    console.log(exerciseModel);
    this.newExercise.Id = exerciseModel.Id;
    this.newExercise.Name = exerciseModel.Name;
    this.newExercise.ImageUrl = exerciseModel.ImageUrl;
    this.newExercise.Minutes = exerciseModel.Minutes;
    this.newExercise.Seconds = exerciseModel.Seconds;
    this.newExercise.BeatsPerMinute = exerciseModel.BeatsPerMinute;
    this.newExercise.HasMetronome = exerciseModel.HasMetronome;
    this.newExercise.OrderIndex = exerciseModel.OrderIndex;
    this.newExercise.BpmToIncrease = exerciseModel.BpmToIncrease;
  }

  public copyExercise(originalExercise: ExerciseModel) {
    const originalExerciseStringfied = JSON.stringify(originalExercise);
    const originalExerciseParsed = JSON.parse(originalExerciseStringfied);

    console.log(originalExerciseParsed);

    const copyExercise = new ExerciseModel();
    copyExercise.Id = '';
    copyExercise.Name = originalExerciseParsed.name + ' (copy)';
    copyExercise.ImageUrl = originalExerciseParsed.imageUrl;
    copyExercise.Minutes = originalExerciseParsed.minutes;
    copyExercise.Seconds = originalExerciseParsed.seconds;
    copyExercise.BeatsPerMinute = originalExerciseParsed.beatsPerMinute;
    copyExercise.HasMetronome = originalExerciseParsed.hasMetronome;
    copyExercise.OrderIndex = this.trainingModel.Exercises.length + 1;
    copyExercise.BpmToIncrease = originalExerciseParsed.bpmToIncrease;

    this.fillNewExercise(copyExercise);
    this.goToNewExercise();
  }

  public goToNewExercise() {
    this.trainingTabDisabled = true;
    this.newExerciseVisible = true;
    this.selectedTabIndex = 1;
  }

  public addNewExercise() {
    if (this.newExercise.IsValid()) {
      this.newExercise.OrderIndex = this.trainingModel.Exercises.length + 1;
      this.trainingModel.Exercises.push(this.newExercise);
      this.trainingModel.SortExercises();
      this.newExercise = new ExerciseModel();
      this.trainingTabDisabled = false;
      this.newExerciseVisible = false;
      this.selectedTabIndex = 0;
    } else {
      this.snackBar.open('Verifique os campos obrigatórios', 'X', { duration: 2500 });
    }
  }

  public editExercise(exerciseModel: ExerciseModel) {
    this.fillNewExercise(exerciseModel);
    const index = this.trainingModel.Exercises.findIndex(x =>
      x.Id === exerciseModel.Id && x.Name === exerciseModel.Name);
    this.trainingModel.Exercises.splice(index, 1);
    this.goToNewExercise();
  }

  public removeExerciseFromTraining(exerciseModel: ExerciseModel) {
    this.exerciseIdsToDelete.push(exerciseModel.Id);
    const index = this.trainingModel.Exercises.findIndex(x =>
      x.Id === exerciseModel.Id && x.Name === exerciseModel.Name);
    this.trainingModel.Exercises.splice(index, 1);
  }

  public forgetMan() {
    this.newExercise = new ExerciseModel();
    this.trainingTabDisabled = false;
    this.newExerciseVisible = false;
    this.selectedTabIndex = 0;
  }

  public saveTraining() {
    if (this.trainingModel.Id === undefined || this.trainingModel.Id === null || this.trainingModel.Id === '') {
      this.trainingService.saveTraining(this.trainingModel).then((objectSaved) => {
        this.enrollCurrentUserOnTraining(objectSaved.id);
        this.showMessage('Treino salvo com sucesso!', 'sucess');
        this.router.navigate(['training/play', objectSaved.id]);
      }).catch((error) => {
        this.showMessage('Ocorreu um erro ao salvar o treino', 'error');
        console.log(error);
      });
    } else {
      this.exerciseIdsToDelete.forEach(exerciseId => {
        this.trainingService.removeExerciseFromTraining(exerciseId, this.trainingId);
      });
      this.trainingService.updateTraining(this.trainingModel).then((objectUpdated) => {
        this.showMessage('Treino atualizado com sucesso!', 'sucess');
        this.router.navigate(['training/play', objectUpdated.id]);
      }).catch((error) => {
        this.showMessage('Ocorreu um erro ao atualizar o treino', 'error');
        console.log(error);
      });
    }
  }

  public enrollCurrentUserOnTraining(trainingId: string) {
    this.userService.getUserByUserEmail(this.userEmail).then((userReturned) => {
      this.userService.enrollUserInTraining(userReturned.Id, trainingId).then((objectSaved) => {
        console.log('Usuário matriculado com sucesso');
      }).catch((error) => {
        console.log('Deu erro ao matricular o usuário');
        console.log(error);
      });
    }).catch((error) => {
      console.log('Deu erro em algum lugar');
      console.log(error);
    });

  }

  public showMessage(message: string, theme: string) {
    this.snackBar.open(message, 'X', {
      duration: 5000,
      panelClass: [theme],
      verticalPosition: 'top',
    });
  }

  public inputFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      console.log(file);

      const myReader = new FileReader();
      myReader.onloadend = (e) => {
        this.newExercise.ImageFile = this.trainingService.parseImageFileBase64ToParseFile(myReader.result, file.name);
        this.newExercise.ImageUrl = myReader.result;
        console.log(this.newExercise.ImageFile);
      };
      myReader.readAsDataURL(file);
    }
  }

  public shouldShowImageUploadCloud() {
    return this.newExercise.ImageUrl === '';
  }

  public drop(event: CdkDragDrop<ExerciseModel[]>) {
    moveItemInArray(this.trainingModel.Exercises, event.previousIndex, event.currentIndex);
    for (let index = 0; index < this.trainingModel.Exercises.length; index++) {
      this.trainingModel.Exercises[index].OrderIndex = index;
    }
    console.log(this.trainingModel.Exercises);
  }

  public getAllExercises() {
    const exercises = this.trainingService.getAllExercises().then((exercisesResult) => {
      console.log(exercisesResult);
    }).catch((error) => {
      console.log(error);
    });
  }

}

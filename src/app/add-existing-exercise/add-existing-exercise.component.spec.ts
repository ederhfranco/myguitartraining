import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddExistingExerciseComponent } from './add-existing-exercise.component';

describe('AddExistingExerciseComponent', () => {
  let component: AddExistingExerciseComponent;
  let fixture: ComponentFixture<AddExistingExerciseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddExistingExerciseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddExistingExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

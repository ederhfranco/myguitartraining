import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public userEmail = '';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public signIn() {
    localStorage.setItem('currentUserEmail', this.userEmail);
    this.router.navigate(['home']);
  }

}

import { UserService } from './shared/services/user.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MetronomeComponent } from './metronome/metronome.component';
import { TimerComponent } from './timer/timer.component';
import { PlayerComponent } from './player/player.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { TrainingComponent } from './training/training.component';
import { environment } from './../environments/environment';
import { TrainingService } from './shared/services/training.service';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginGuard } from './shared/infrastructure/guard/login.guard';
import { PlayTrainingPageComponent } from './play-training-page/play-training-page.component';
import { RegisterTrainingPageComponent } from './register-training-page/register-training-page.component';
import { MatButtonModule, MatFormFieldModule, MatInputModule,
  MatRippleModule, MatCheckboxModule, MatSnackBarModule } from '@angular/material';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material/card';
import { AddExistingExerciseComponent } from './add-existing-exercise/add-existing-exercise.component';
import { ExecutionCounterComponent } from './execution-counter/execution-counter.component';
import { FreeTrainingComponent } from './free-training/free-training.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ExerciseCardComponent } from './free-training/exercise-card/exercise-card.component';
import { TransitionCounterComponent } from './training/transition-counter/transition-counter.component';
import { SystemMenuComponent } from './system-menu/system-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    MetronomeComponent,
    TimerComponent,
    PlayerComponent,
    ExerciseComponent,
    TrainingComponent,
    HomePageComponent,
    LoginPageComponent,
    PlayTrainingPageComponent,
    RegisterTrainingPageComponent,
    AddExistingExerciseComponent,
    ExecutionCounterComponent,
    FreeTrainingComponent,
    ExerciseCardComponent,
    TransitionCounterComponent,
    SystemMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCheckboxModule,
    MatTabsModule,
    MatListModule,
    MatIconModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSnackBarModule,
    DragDropModule,
    MatCardModule,
    MatSidenavModule,
    MatToolbarModule,
  ],
  providers: [TrainingService, UserService, LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

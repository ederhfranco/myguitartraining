import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-transition-counter',
  templateUrl: './transition-counter.component.html',
  styleUrls: ['./transition-counter.component.scss']
})
export class TransitionCounterComponent implements OnInit {


  public countdownNumberEl: string;
  private countdown = 4;
  private timerManager: any;

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.countdownNumberEl = this.countdown.toString();
    this.go();
    setTimeout(() => {
      this.stop();
    }, 4000);
  }

  public stop() {
    clearTimeout(this.timerManager);
  }

  public go() {
    console.log(this.countdown);
    let value = '';
    if (this.countdown - 1 === 0) {
      this.countdown = 4;
      value = 'Go!';
    } else {
      this.countdown = --this.countdown;
      value = this.countdown.toString();
    }

    this.countdownNumberEl = value;
    this.changeDetector.detectChanges();
    this.timerManager = setTimeout(this.go.bind(this), 1000);
  }
}

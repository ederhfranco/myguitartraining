import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitionCounterComponent } from './transition-counter.component';

describe('TransitionCounterComponent', () => {
  let component: TransitionCounterComponent;
  let fixture: ComponentFixture<TransitionCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransitionCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitionCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

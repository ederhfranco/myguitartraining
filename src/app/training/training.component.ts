import { ExerciseModel } from './../shared/models/exercise.model';
import { TrainingModel } from './../shared/models/training.model';
import { Component, OnInit, Input, ViewChild, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  @Input()
  public trainingModel: TrainingModel;

  @Input()
  public showTrainingFinished = true;

  @ViewChild('exerciseComponent')
  public exerciseComponent: any;

  public imageLoading = './../../assets/partitura-load.gif';

  public trainingFinished = false;
  public trainingFinishedImage = './../../assets/rocky-training.gif';

  public showInitialLoading = false;
  public isFirstExercise = true;

  public bpmToAdd = 0;

  private running = false;

  constructor(private changeDetector: ChangeDetectorRef) {
    this.trainingModel = new TrainingModel();
  }

  ngOnInit() {
  }

  public refresh() {
    this.exerciseComponent.refreshSoft();
    this.changeDetector.detectChanges();
  }

  public pause() {
    this.running = false;
    this.exerciseComponent.pause();
  }
  public play() {
    if (this.isFirstExercise) {
      this.exerciseComponent.refresh();
      this.isFirstExercise = false;
    } else {
      if (!this.running) {
        this.exerciseComponent.play();
      }
    }
    this.running = true;
  }

  public nextExercise() {
    const endOfArray = this.trainingModel.SetNextExercise();

    if (endOfArray) {
      this.pause();
      this.finishTraining();
      return;
    }

    this.changeDetector.detectChanges();
    this.exerciseComponent.refresh();
  }
  public previousExercise() {
    this.trainingModel.SetPreviousExercise();
    this.changeDetector.detectChanges();
    this.exerciseComponent.refresh();
  }

  public isRunning() {
    return this.running;
  }

  private finishTraining() {
    this.trainingFinished = true;
    this.trainingModel.CurrentExercise = this.trainingModel.Exercises[0];
  }

}

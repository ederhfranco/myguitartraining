import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from './shared/infrastructure/guard/login.guard';
import { PlayTrainingPageComponent } from './play-training-page/play-training-page.component';
import { RegisterTrainingPageComponent } from './register-training-page/register-training-page.component';
import { ExecutionCounterComponent } from './execution-counter/execution-counter.component';
import { FreeTrainingComponent } from './free-training/free-training.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent, canActivate: [LoginGuard] },
  { path: '', component: HomePageComponent, canActivate: [LoginGuard] },
  { path: 'login', component: LoginPageComponent },
  { path: 'training/play/:trainingId', component: PlayTrainingPageComponent, canActivate: [LoginGuard] },
  { path: 'training/new', component: RegisterTrainingPageComponent, canActivate: [LoginGuard] },
  { path: 'training/edit/:trainingId', component: RegisterTrainingPageComponent, canActivate: [LoginGuard] },
  { path: 'execution-counter', component: ExecutionCounterComponent, canActivate: [LoginGuard] },
  { path: 'free-training', component: FreeTrainingComponent, canActivate: [LoginGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

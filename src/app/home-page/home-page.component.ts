import { UserModel } from './../shared/models/user.model';
import { UserService } from './../shared/services/user.service';
import { Component, OnInit, Input } from '@angular/core';
import { User } from 'parse';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  @Input()
  public userEmail: string;

  public user: UserModel;
  public userTrainings: any;

  constructor(private userService: UserService,
    private router: Router) {
    this.user = new UserModel();
  }

  ngOnInit() {
    this.userEmail = localStorage.getItem('currentUserEmail');
    if (this.userEmail) {
      this.getTrainingsToUser();
    }
  }

  private getTrainingsToUser() {
    this.userService.getUserByUserEmail(this.userEmail).then((userReturned) => {
      this.user = userReturned;
      this.userService.getTrainingsByUser(this.user.Id).then((trainingsResult) => {
        this.userTrainings = trainingsResult;
      });
    }).catch((error) => {
      console.log('Deu erro em algum lugar');
      console.log(error);
    });
  }

  public goToTraining(trainingId: string) {
    this.router.navigate(['training/play', trainingId]);
  }

  public goToTrainingEdit(trainingId: string) {
    this.router.navigate(['training/edit', trainingId]);
  }
}

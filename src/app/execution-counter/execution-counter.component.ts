import { Component, OnInit, HostListener } from '@angular/core';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37
}

@Component({
  selector: 'app-execution-counter',
  templateUrl: './execution-counter.component.html',
  styleUrls: ['./execution-counter.component.scss']
})
export class ExecutionCounterComponent implements OnInit {

  public counter: number = 0;
  public goal: number = 10;
  public imageCompleteCloud = './../../assets/complete.png';

  constructor() { }

  ngOnInit() {
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    
    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      this.incrementCounter();
    }

    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      this.decrementCounter();
    }
  }

  public incrementCounter() {
    if (this.counter < this.goal) {
      this.counter += 1;
    }
  }

  public decrementCounter() {
    if (this.counter > 0) {
      this.counter -= 1;
    }
  }

  public shouldShowCompleteImage(): boolean {
    return this.counter === this.goal && this.counter > 0;
  }

}

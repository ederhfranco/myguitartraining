import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionCounterComponent } from './execution-counter.component';

describe('ExecutionCounterComponent', () => {
  let component: ExecutionCounterComponent;
  let fixture: ComponentFixture<ExecutionCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { AnonymousSubject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-metronome',
  templateUrl: './metronome.component.html',
  styleUrls: ['./metronome.component.scss']
})
export class MetronomeComponent implements OnInit {

  private interval: number;
  private timerManager: any;
  private bip: any;
  private minuteInMilisecondsConstant = 60000;

  @Input()
  public beatsPerMinute: number;

  @Input()
  public enabled: true;

  public running = false;

  constructor(private changeDetector: ChangeDetectorRef) {
    this.bip = new Audio();
    this.bip.src = './../../../assets/sounds/bip.flac';
    this.bip.load();
  }

  ngOnInit() {
  }

  public refresh(): void {
    this.setInterval();
    this.changeDetector.detectChanges();
  }

  public start(): void {
    if (!this.running && this.enabled) {
      this.running = true;
      this.setInterval();
      this.tick();
    }
  }

  public stop() {
    this.running = false;
    clearTimeout(this.timerManager);
  }

  private setInterval() {
    this.interval = this.minuteInMilisecondsConstant / this.beatsPerMinute;
  }

  private tick() {
    this.bip.play();
    this.timerManager = setTimeout(this.tick.bind(this), this.interval);
  }
}

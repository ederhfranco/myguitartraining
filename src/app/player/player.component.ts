import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

  public playing = false;

  /*
    The parent component is responsible for stopping the timer and metronome,
    so this component will only trigger events so that the parent component
    can interpret those events and perform the proper operations
  */
  @Output()
  private playEvent: EventEmitter<any> = new EventEmitter();
  @Output()
  private pauseEvent: EventEmitter<any> = new EventEmitter();
  @Output()
  private nextEvent: EventEmitter<any> = new EventEmitter();
  @Output()
  private previousEvent: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public playClick() {
    this.playing = true;
    this.playEvent.emit();
  }

  public pauseClick() {
    this.playing = false;
    this.pauseEvent.emit();
  }

  public nextClick() {
    this.playing = true;
    this.nextEvent.emit();
  }

  public previousClick() {
    this.playing = true;
    this.previousEvent.emit();
  }

}

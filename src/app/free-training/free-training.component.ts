import { ExerciseModel } from './../shared/models/exercise.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TrainingModel } from '../shared/models/training.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-free-training',
  templateUrl: './free-training.component.html',
  styleUrls: ['./free-training.component.scss']
})
export class FreeTrainingComponent implements OnInit {

  public showFiller = false;
  public trainingModel: TrainingModel;
  public showInitialLoading = false;

  public amountOfExercises: number;
  public durationMinutes = 0;
  public durationSeconds = 0;
  public durationIntervalSeconds = 0;
  public bpmExercise = 0;

  @ViewChild('trainingComponent')
  public trainingComponent: any;

  constructor() { }

  ngOnInit() {
    this.trainingModel = new TrainingModel();
    this.generateFreeTraining();
  }

  public generateFreeTraining() {
    console.log(this.trainingComponent.isRunning());
    if (this.trainingComponent.isRunning()) {
      console.log('vai parar');
      this.trainingComponent.pause();
    }

    this.trainingModel = new TrainingModel();

    for (let index = 1; index <= this.amountOfExercises; index++) {
      const exercise = new ExerciseModel();
      exercise.Minutes = this.durationMinutes;
      exercise.Seconds = this.durationSeconds;
      exercise.BeatsPerMinute = this.bpmExercise;
      exercise.HasMetronome = true;
      // exercise.Name = 'Exercício ' + index;
      exercise.Name = 'Exercício ';
      this.trainingModel.Exercises.push(exercise);

      if (this.durationIntervalSeconds > 5 && index < this.amountOfExercises) {
        const interval = new ExerciseModel();
        interval.Minutes = 0;
        interval.Seconds = this.durationIntervalSeconds;
        interval.Name = 'Pausa';
        this.trainingModel.Exercises.push(interval);
      }
    }

    this.trainingModel.CurrentExercise = this.trainingModel.Exercises[0];

    console.log(this.trainingModel);

    this.trainingComponent.refresh();
  }

  public showTrainingComponent() {
    return this.trainingModel.Exercises.length === 0;
  }

  public drop(event: CdkDragDrop<ExerciseModel[]>) {
    moveItemInArray(this.trainingModel.Exercises, event.previousIndex, event.currentIndex);
    for (let index = 0; index < this.trainingModel.Exercises.length; index++) {
      this.trainingModel.Exercises[index].OrderIndex = index;
    }
    this.trainingModel.SortExercises();
    this.trainingComponent.refresh();
    console.log(this.trainingModel.Exercises);
  }

  public removeExerciseFromTraining(exerciseModel: ExerciseModel) {
    const index = this.trainingModel.Exercises.findIndex(x =>
      x.OrderIndex === exerciseModel.OrderIndex && x.Name === exerciseModel.Name);
    this.trainingModel.Exercises.splice(index, 1);
  }

}

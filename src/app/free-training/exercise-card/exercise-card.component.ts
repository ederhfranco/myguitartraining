import { Component, OnInit, Input } from '@angular/core';
import { ExerciseModel } from 'src/app/shared/models/exercise.model';

@Component({
  selector: 'app-exercise-card',
  templateUrl: './exercise-card.component.html',
  styleUrls: ['./exercise-card.component.scss']
})
export class ExerciseCardComponent implements OnInit {

  @Input()
  public exercise: ExerciseModel;

  constructor() { }

  ngOnInit() {
  }

}

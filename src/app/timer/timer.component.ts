import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';
import { StatusTimerEnum } from './shared/enums/status-timer.enum';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  private statusTimer: StatusTimerEnum;
  private timerManager: any;

  @Output()
  private finishedTimerEvent: EventEmitter<any> = new EventEmitter();

  @Input()
  public minutes: number;
  @Input()
  public seconds: number;

  public minutesText: string;
  public secondsText: string;

  constructor() {
  }

  ngOnInit() {
    this.minutes = 0;
    this.seconds = 0;
    this.setTextVariables();
  }

  public refresh(minutes: number, seconds: number) {
    this.minutes = minutes;
    this.seconds = seconds;
    this.setTextVariables();
  }

  public start() {
    this.setTextVariables();
    if (this.statusTimer !== StatusTimerEnum.Running) {
      setTimeout(() => {
        this.statusTimer = StatusTimerEnum.Running;
        this.decreaseTimer();
      }, 1000);
    }
  }

  public pause() {
    this.statusTimer = StatusTimerEnum.Paused;
    clearTimeout(this.timerManager);
  }

  private decreaseTimer() {
    if (this.shouldStop()) {
      return;
    }

    if (this.seconds === 0) {
      if (this.minutes > 0) {
        this.seconds = 59;
        this.minutes -= 1;
      }
    } else {
      this.seconds -= 1;
    }
    this.setTextVariables();
    this.timerManager = setTimeout(this.decreaseTimer.bind(this), 1000);
  }

  private setTextVariables() {
    const minutes = this.minutes ? this.minutes : 0;
    const seconds = this.seconds ? this.seconds : 0;

    this.minutesText = minutes < 10 ? '0' + minutes.toString() : minutes.toString();
    this.secondsText = seconds < 10 ? '0' + seconds.toString() : seconds.toString();
  }

  private shouldStop() {
    return this.isFinished() || (this.statusTimer === StatusTimerEnum.Paused);
  }

  private isFinished() {
    if (this.minutes === 0 && this.seconds === 0) {
      this.finishedTimerEvent.emit();
      return true;
    }
  }

}
